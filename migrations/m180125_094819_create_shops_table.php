<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shops`.
 */
class m180125_094819_create_shops_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shops', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'regionId' => $this->integer(),
            'city' => $this->string(255),
            'address' => $this->string(255),
            'userId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shops');
    }
}
