<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Shops;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ImportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($path)
    {
        $f = fopen($path, "r");
        while (($data = fgetcsv($f, 1000, ",", '"')) !== FALSE) {
            $shop = new Shops();

            $shop->regionId = $data[0];
            $shop->title = $data[1];
            $shop->city = $data[2];
            $shop->address = $data[3];
            $shop->userId = $data[4];

            if($shop->validate()){
                $shop->save();
            }
        }
    }
}
