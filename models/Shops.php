<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property int $id
 * @property string $title
 * @property int $regionId
 * @property string $city
 * @property string $address
 * @property int $userId
 */
class Shops extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['regionId', 'userId'], 'integer'],
            [['title', 'city', 'address'], 'string', 'max' => 255],
            [['regionId', 'userId', 'title', 'city', 'address'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'regionId' => 'Region ID',
            'city' => 'City',
            'address' => 'Address',
            'userId' => 'User ID',
        ];
    }
}
